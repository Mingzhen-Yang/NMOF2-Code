% exRankcor.m -- version 2010-01-09
%% example rank correlation
p = 3; N = 1000; 
X = randn(N,p);
X = X * (diag(1./std(X)));
X = X - ones(N,1)*mean(X);
plotmatrix(X); corrcoef(X)

%% induce rank correlation: Spearman
rhoS = 0.9;   % correlation between any two assets

% set rank correlation matrix
Mrank = ones(p,p) * rhoS;
Mrank(1:(p + 1):(p * p)) = 1;

% compute corresponding linear correlation matrix
M = 2*sin(pi/6.*Mrank);

% compute cholesky factor
C = chol(M);

% induce correlation, check
Xc = X * C;
plotmatrix(Xc); 

% check
corr(Xc,'type','Pearson')
corr(Xc,'type','Spearman')

sd = 5; Xc(:,1) = sd * Xc(:,1);
Z = exp(Xc);
% check
corr(Z,'type','Pearson')
corr(Z,'type','Spearman')