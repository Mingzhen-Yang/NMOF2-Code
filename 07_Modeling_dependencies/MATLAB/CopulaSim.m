function [u,x,Finv,c] = CopulaSim(n,copula,p)
% CopulaSim.m  -- version 2011-01-06
%   n ....... number of samples
%   copula .. name of copula (Frank,Clayton,Gumbel or Gaussian)
%   p ....... parameter of the copula

% marginal
Finv = @(u) norminv(u);
% densities
switch upper(copula)
  case 'FRANK'         % parameter: p<>0
    c = @(u,v,p) p*(1-exp(-p)).*exp(-p*(u+v)) ./ ...
        (exp(-p*(u+v)) - exp(-p*u) - exp(-p*v) + exp(-p)).^2;
  case 'CLAYTON'       % parameter: p>= -1; p~=0
    c = @(u,v,p) (1+p) ./ ((u.*v).^(1+p) .*(u.^(-p) + ...
                                       v.^(-p)-1).^(2+1/p) );
  case 'GUMBEL'        % parameter:  p >= 1
    c = @(u,v,p) ( (-log(u)).^(p-1) .* (-log(v)).^(p-1) .* ...
        ((-log(u)).^p + (-log(v)).^p).^(1/p-2)) ./ ...
        (u.*v.*exp( ((-log(u)).^p + (-log(v)).^p).^(1/p)));
  otherwise % Gaussian % parameter: -1 < p < 1
    c = @(u,v,p) 1/sqrt(1-p^2) * exp((norminv(u).^2 + ...
        norminv(v).^2)/(2) + (2*p*norminv(u).*norminv(v) - ...
        norminv(u).^2-norminv(v).^2) / (2*(1-p^2)));
end
% Metropolis
u = NaN(n,2);
u(1,:) = rand(1,2);
f_i = c(u(1,1),u(1,2),p);
for i = 2:n;
    while isnan(u(i,:))
        u_new = rand(1,2);
        f_new = c(u_new(1),u_new(2),p);
        if rand < f_new/f_i
            u(i,:) = u_new;
            f_i = f_new;
        end
    end
end
x = Finv(u);

