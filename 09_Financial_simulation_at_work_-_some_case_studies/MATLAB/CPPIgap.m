function [V C B F E] = CPPIgap(S, m, G, r_c, gap)
% CPPIgap.m  version -- 2010-12-22
% S   .. stock price series t = 0 .. T
% G   .. guarantueed payback amount
% m   .. multiplier
% r_c .. cumulated save return over entire horizon
% gap .. readjustment frequency; if blank: 1 = always
if nargin < 5, gap = 1;  end;

% --initial setting
T = length(S)-1;
t = 0:T;

V    = zeros(T+1,1);
V(1) = G;

F = G*exp(-r_c*((T-t)/T))';

C = zeros(T+1,1);
B = zeros(T+1,1);
n = zeros(T+1,1);  % number of risky assets

% --development over time
for tau=1:T     % tau = t+1
    C(tau)   = V(tau)-F(tau);

    if mod(tau-1,gap)==0  % re-adjust now
        E(tau)   = min(m * C(tau), V(tau));
        n(tau)   = E(tau) / S(tau);
        B(tau)   = V(tau) - E(tau);
    else
        n(tau)  = n(tau-1);
        E(tau)  = V(tau) - B(tau);
    end;

    B(tau+1) = B(tau)*exp(r_c/T);
    V(tau+1) = n(tau)*S(tau+1) + B(tau+1);
end