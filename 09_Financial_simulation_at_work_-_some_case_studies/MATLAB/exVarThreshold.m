clear all, close all
addpath ..\..  ..\M  C:\NMOFbook\C-RandomNumberGeneration\M
FigPars

% exVaRThreshold.m  -- version 2011-01-10
N     = 1250;
Nruns = 1000;
alpha = logspace(0,1,18).^2 / 1e3; % [.001 .005 .01 .025 .05 .075 .1];
mOverNSet  = [ .01 .025 .05 .1];
sizemOverNSet = length(mOverNSet)+1;
mSet = ceil(mOverNSet*N);

%% ..... normal distribution
VaR_normal  = nan(Nruns,length(alpha),length(mSet)+1);
for run = 1:Nruns
    r = randn(N,1);
    for j = 1:length(mSet)
        m = mSet(j);
        VaR_normal(run,:,j) = VaRHill(r,m,alpha);
    end;
    VaR_normal(run,:,end) = quantile(r,alpha);
end;

%% ..... student t distribution
VaR_student = nan(Nruns,length(alpha),length(mSet)+1);
for run = 1:Nruns
    r = tinv(rand(N,1),5);
    for j = 1:length(mSet)
        m = mSet(j);
        VaR_student(run,:,j) = VaRHill(r,m,alpha);
    end;
    VaR_student(run,:,end) = quantile(r,alpha);
end;

%% ..... via Cornish Fisher approximation
VaR_CF      = nan(Nruns,length(alpha),length(mSet)+1);
for run = 1:Nruns
    r = CornishFisherSimulation(0,1,-.3,3,N);
    for j = 1:length(mSet)
        m = mSet(j);
        VaR_CF(run,:,j) = VaRHill(r,m,alpha);
    end;
    VaR_CF(run,:,end) = quantile(r,alpha);
end;

save exVaRResults VaR_CF VaR_normal VaR_student


%% evaluation

load exVaRResults

mvarnorm = reshape(median(VaR_normal),[],sizemOverNSet,1)';
mvarstud = reshape(median(VaR_student),[],sizemOverNSet,1)';
mvarcf   = reshape(median(VaR_CF),[],sizemOverNSet,1)';

mvarnormt = mvarnorm ./ repmat(mvarnorm(end,:),sizemOverNSet,1)
mvarstudt = mvarstud ./ repmat(mvarstud(end,:),sizemOverNSet,1)
mvarcft   = mvarcf   ./ repmat(mvarcf(end,:),sizemOverNSet,1)


%% plots
Gtone(1,:) = 0.1*[1 1 1];  grinc = .18*[1 1 1];
for i = 2:sizemOverNSet-1
    Gtone(i,:) = Gtone(i-1,:) + grinc;
end

figure(1)
gtone = 0.1*[1 1 1]; grinc = .15*[1 1 1];
subplot(2,3,1),
for i = 1:sizemOverNSet-1
    plot(alpha,mvarnormt(i,:),'k-','LineWidth',LW,'Color',Gtone(i,:)), hold all
end
set(gca,'FontSize',FS);
title('normal distribution')
xlabel('VaR confidence level, \alpha');
ylabel('VaR^{Hill}_{m/N} / VaR^{theo}')

subplot(2,3,2)
for i = 1:sizemOverNSet-1
    plot(alpha,mvarstudt(i,:),'k-','LineWidth',LW,'Color',Gtone(i,:)), hold all
end
set(gca,'FontSize',FS);
xlabel('VaR confidence level, \alpha');
title('student t distribution, \nu=5')

subplot(2,3,3)
for i = 1:sizemOverNSet-1
    plot(alpha,mvarcft(i,:),'k-','LineWidth',LW,'Color',Gtone(i,:)), hold all
end
set(gca,'FontSize',FS);
xlabel('VaR confidence level, \alpha');
title('Cornish Fisher; {\it S} = -0.3')
legend({num2str(mOverNSet')},'location','north')

%  figure(1); print -depsc ..\Figs\VaRThresholdChoice.eps

