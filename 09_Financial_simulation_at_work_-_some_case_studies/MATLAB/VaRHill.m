function [VaR ES ksi] = VaRHill(r,m,a);
% VaRHill.m  -- version 2011-01-06
%   r ... historical returns
%   m ... number of largest losses used
%   a ... probability VaR is exceeded

% --adjust parameters where necessary
if m <  1;  m = ceil(m*length(r)); end;
if a >  1;  a = a/100; end
if a > .5;  a = 1-a;   end

% --compute Hill estimator
r_order = sort(r);
ksi = sum(log(r_order(1:m)/r_order(m+1))) / m;

% --compute the VaR and ES
VaR = r_order(m+1) * ( (m/length(r)) ./ a ) .^ksi;
ES  = VaR ./ ( 1-ksi);