function [call,payoff] = callBSMMC4(S,X,tau,r,q,v,M,N)
% callBSMMC4.m -- version 2010-12-08
% S     = spot
% X     = strike
% tau   = time to maturity
% r     = riskfree rate
% q     = dividend yield
% v     = volatility^2
% M     = time steps
% N     = number of paths
dt = tau/M;
g1 = (r - q - v/2)*dt; g2 = sqrt(v*dt);
s = log(S);
ee = g2 * randn(M, N);
z = cumsum(g1+ee, 1) + s;  % cumsum(...,1) in case of M=1!
S = exp(z(end, :));
payoff = max(S-X, 0);
z = cumsum(g1-ee, 1) + s;  % cumsum(...,1) in case of M=1!
S = exp(z(end, :));
payoff = payoff + max(S-X, 0);
payoff = payoff/2;
payoff = exp(-r*tau) * payoff;
call = mean(payoff);
