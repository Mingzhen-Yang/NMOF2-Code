function call = callBSM(S,X,tau,r,q,v)
% callBSM.m -- version 2010-12-08
% S   = spot
% X   = strike
% tau = time to mat
% r   = riskfree rate
% q   = dividend yield
% v   = volatility^2
d1 = ( log(S/X) + (r - q + v/2)*tau ) / (sqrt(v*tau));
d2 = d1 - sqrt(v*tau);
call = S*exp(-q*tau)*normcdf(d1) - X*exp(-r*tau)*normcdf(d2);
