function [call,payoff] = callBSMMC(S,X,tau,r,q,v,M,N)
% callBSMMC.m -- version 2010-12-10
% S   = spot
% X   = strike
% tau = time to maturity
% r   = riskfree rate
% q   = dividend yield
% v   = volatility^2
% M   = time steps
% N   = number of paths
S = pricepaths(S,tau,r,q,v,M,N);
payoff = max((S(end,:)-X),0);
payoff = exp(-r*tau) * payoff;
call = mean(payoff);