function vv = VDC(k,b)
% VDC.m -- version 2010-12-08
nD = 1 + floor(log(max(k))/log(b)); % required digits
nN = length(k);                     % number of VDC numbers
vv = zeros(nN,nD);
for i = nD:-1:1
    vv(:,i) = mod(k,b);
    if i>1; k = fix(k/b); end
end
ex = b .^ (nD:-1:1);
vv = vv ./ (ones(nN,1)*ex);
vv = sum(vv,2);