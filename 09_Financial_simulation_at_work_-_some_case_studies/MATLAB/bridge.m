function b = bridge(t0,ttau,z0,ztau,M)
% bridge.m -- version 2010-12-08
dt = (ttau-t0)/M; vt = linspace(t0,ttau,M+1)';
vz = [0; cumsum(randn(M,1) * sqrt(dt))];
b = z0 + vz - (vt - t0)/(ttau - t0) .* (vz(M + 1) - ztau + z0);
