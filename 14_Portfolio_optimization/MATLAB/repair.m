% repair.m  -- version  2010-12-10
% --compute eigenvectors/-values
[V, D]  = eig(C);

% --replace negative eigenvalues by zero
D       = max(D, 0);

% --reconstruct correlation matrix
CC      = V * D * V';

% --rescale correlation matrix
S       = 1 ./ sqrt(diag(CC));
SS      = S * S';
C       = CC .* SS;
