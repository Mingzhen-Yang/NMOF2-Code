% minVar.m -- version 2010-12-12

% generate artificial returns data
ns = 60;   % number of scenarios
na = 10;   % number of assets
R = 0.005 + randn(ns, na) * 0.015;
Q = 2 * cov(R);

% set up
c = zeros(1,na);
A = ones(1,na); a = 1;
B = -eye(na); b = zeros(na,1);

% solution
w = quadprog(Q,c,B,b,A,a);

% check constraints
sum(w)
all(w>=0)
