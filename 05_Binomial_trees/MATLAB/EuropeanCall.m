function C0 = EuropeanCall(S0,X,r,T,sigma,M)
% EuropeanCall.m -- version 2010-12-28
% compute constants
f7 = 1;  dt = T / M;  v = exp(-r * dt);
u = exp(sigma*sqrt(dt));   d = 1 /u;
p = (exp(r * dt) - d) / (u - d);

% initialize asset prices at maturity (period M)
S = zeros(M + 1,1);
S(f7+0) = S0 * d^M;
for j = 1:M
    S(f7+j) = S(f7+j - 1) * u / d;
end

% initialize option values at maturity (period M)
C = max(S - X, 0);

% step back through the tree
for i = M-1:-1:0
    for j = 0:i
        C(f7+j) = v * (p * C(f7+j + 1) + (1-p) * C(f7+j));
    end
end
C0 = C(f7+0);