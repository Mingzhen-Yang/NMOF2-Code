function C0 = EuropeanCallBE(S0,X,r,T,sigma,M)
% EuropeanCallBE.m  -- version: 2011-05-15
% compute constants
dt = T / M;
u = exp(sigma*sqrt(dt));   d = 1 /u;
p = (exp(r * dt) - d) / (u - d);

% initialise asset prices at maturity (period M)
C = max(S0*d.^((M:-1:0)').*u.^((0:M)') - X,0);

% log/cumsum version
csl = cumsum(log([1; [1:M]']));
tmp = csl(M+1) - csl - csl(M+1:-1:1) + log(p)*((0:M)') + ...
      log(1-p)*((M:-1:0)');
C0 = exp(-r*T)*sum(exp(tmp).*C);