% ExLSsvd.m  -- version 1995-11-27
A = [1 1; 1 2; 1 3; 1 4]; b = [2 1 1 1]';
[m,n] = size(A);
[U,S,V] = svd(A);
c = U'*b;  c1 = c(1:n);  c2 = c(n+1:m);
sv = diag(S);
z1 = c1./sv;
x = V*z1
sigma2 = c2'*c2/(m-n)
S = V*diag(sv.^(-2))*V';
Mcov = sigma2*S
