function [S_bs,r_bs] = bootstrapPrice(S,n,b,S_0)
% bootstrapPrice.m  -- version 2011-01-06
% 	returns one bootstrap price path
%   S ..... original price series
%   n ..... length of bootstrap sample x
%   b ..... block length
%   S_0 ... initial price for simulation (S(T) if not provided)
if nargin < 4, S_0 = S(end,:); end

r    = diff(log(S));               % log returns
r_bs = bootstrap(r,n,b);
r_bs_c = cumsum([log(S_0); r_bs]);
S_bs = exp(r_bs_c);
