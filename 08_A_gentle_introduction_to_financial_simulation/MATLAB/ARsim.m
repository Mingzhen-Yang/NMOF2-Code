function [r,e] = ARsim(T,mu,sigma,phi)
% ARsim.m  -- version 2019-04-17
%   simulation of AR(p) process

p = length(phi);
e = randn(2*T+p,1) * sigma;
r = ones(2*T+p,1) * mu /(1-sum(phi(:)));
for t = p+(1:(2*T))
    r(t) = mu + r(t-(1:p))' * phi(:) + e(t);
end
r(1:(p+T)) = [];
e(1:(p+T)) = [];

