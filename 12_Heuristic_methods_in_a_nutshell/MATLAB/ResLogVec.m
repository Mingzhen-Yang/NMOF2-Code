% ResLogVec -- version 2018-11-23
fprintf(' Elapsed time %4.1f sec\n',t1);
if exist('TA','var'), P='R'; m=nR*nS; else P='G'; end
w = Data.w;  s = Data.s;
fprintf(' ------------------------')
for i = 1:nRe
    J = find(X(:,i)); sol = sum(w(J));  nJ = numel(J); 
    if abs(sol - s), tag = ' ';  else tag = '*';  end
    if strcmp(P,'R') % -- Rounds
        j = find(~isnan(JB(:,i)),1,'last');
        p = ceil(100*( JB(j,i) / m ));
    else % -- Generations
        p = ceil(100*( JB(i) / nG ));
    end
    fprintf('\n%s Sol = %4i %3i%% %3i el.:',tag,sol,p,nJ);
    for k = 1:nJ, fprintf('%4i',w(J(k))); end;
end, fprintf('\n -------------------------\n'); S = Sol;

