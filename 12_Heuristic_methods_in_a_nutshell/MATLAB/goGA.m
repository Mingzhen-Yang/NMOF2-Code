% goGA.m -- version 2018-11-13
if ~exist('nG','var'),  nG  =  80; end
if ~exist('pM','var'),  pM  = 0.2; end
if ~exist('nP','var'),  nP  = 200; end
if ~exist('nP1','var'), nP1 = 100; end
if ~exist('nP2','var'), nP2 = 200; end
GA = struct('OF',OF,'SP',SP,'nC',nC,'nG',nG,'nP',nP,...
            'nP1',nP1,'nP2',nP2,'pM',pM,'Restarts',nRe);
Sol = NaN(nRe,1);  X = zeros(nC,nRe);     t0 = tic;
FG = NaN(nG,nRe); JB = NaN(1,nRe);
for r = 1:nRe
    [Sol(r),X(:,r),FG(:,r),JB(r)] = GAH(GA,Data);
end, t1 = toc(t0);