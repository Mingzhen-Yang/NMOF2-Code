nRe = 10; tser = 8;  ptime = 5;  nW = 2; Res = zeros(nW,5);
% -- Serial execution ----------------------------
t0 = tic;
pause(tser)      % serial code
for k = 1:nRe    % parallelizable code
    pause(ptime)
end
T1 = toc(t0);    % Time for serial execution
% ------------------------------------------------
pool = parpool; % Create parallel pool
t0 = tic;
% -- Parallel execution --
pause(tser)       % serial code
parfor k = 1:nRe  % parfor replaces for
    pause(ptime)
end
Tp = toc(t0);  p = pool.NumWorkers;   delete(pool)
Sp = T1/Tp;   Ep = Sp/p;

Res(p,:) = [p   T1 Tp Sp Ep];
% ------------------
tlat = 0;
[tser, tpar, theta, Smax] = Amdahl(T1,Tp,p,tlat);
fid = fopen('GilliRes.txt','w'); fid = 1;
fprintf(fid,'\n    p     T1      Tp      Sp      Ep');
fprintf(fid,'\n --------------------------------------\n');
for p = 2:nW
    fprintf(fid,'   %2i ',Res(p,1));
    for i = 2:5
        fprintf(fid,' %6.2f ',Res(p,i));
    end
    fprintf(fid,'\n');
end
%S = fclose(fid);
