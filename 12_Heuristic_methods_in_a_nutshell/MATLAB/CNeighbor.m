function x1 = CNeighbor(x0,j,Data,TA)
% CNeighbor.m  -- version 2018-10-25
x1 = x0;
% Randomly select element j of x0
x1(j) = x1(j) + randn * TA.scale;   int = Data.int;
% Check domain constraints
x1(j) = min(int(j,2), max(int(j,1),x1(j)) ); 
