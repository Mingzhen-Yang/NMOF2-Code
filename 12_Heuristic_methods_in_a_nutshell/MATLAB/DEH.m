function [Fbest,xbest,FG,kbest] = DEH(DE,Data)
% DEH.m  -- version 2018-10-31
 n = Data.n;  nP = DE.nP;   nG = DE.nG;
OF = DE.OF;   SP = DE.SP;  Col = 1:nP;  FG = NaN(nG,1);
[F,P1,Fbest]  = feval(SP,n,nG,OF,Data); % --Starting pop.
for k = 1:nG
    P0 = P1;
    Io = randperm(nP)';         Ic = randperm(4)';
    I  = circshift(Io,Ic(1));   R1 = circshift(Io,Ic(2));
    R2 = circshift(Io,Ic(3));   R3 = circshift(Io,Ic(4));
    % -- Construct mutant array
    Pv = P0(:,R1) + DE.F * (P0(:,R2) - P0(:,R3));
    % -- Crossover
    mPv = rand(n,nP) < DE.CR;
    if DE.oneElemfromPv
       Row = unidrnd(n,1,nP);
       mPv1 = sparse(Row,Col,1,n,nP);
       mPv = mPv | mPv1;
    end
    mP0  = ~mPv;
    Pu(:,I) = P0(:,I).*mP0 + mPv.*Pv;
    % -- Select array to enter new generation
    flag = 0;
    for i = 1:nP
        Ftemp = feval(OF,Pu(:,i),Data);
        if Ftemp <= F(i)
            P1(:,i) = Pu(:,i);
            F(i) = Ftemp;
            if Ftemp < Fbest
                Fbest = Ftemp;  xbest = Pu(:,i); flag = 1;
            end
        else
            P1(:,i) = P0(:,i);
        end
    end
    if flag, FG(k) = Fbest; kbest = k; end
end

