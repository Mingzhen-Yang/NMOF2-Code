% goPS.m  -- version 2018-11-14
if ~exist('nP','var'),   nP = 15; end
if ~exist('nG','var'),   nG = 40; end
if ~exist('c1','var'),   c1 =  2; end
if ~exist('c2','var'),   c2 =  2; end
if ~exist('cv','var'),   cv =  1; end
if ~exist('vmax','var'), vmax =1; end
PS = struct('OF',OF,'SP',SP,'nP',nP,'nG',nG,'c1',c1,...
            'c2',c2,'cv',cv,'vmax',vmax,'Restarts',nRe);
Sol = NaN(nRe,1);     X = zeros(Data.n,nRe);
FG  = zeros(nG,nRe); JB = NaN(1,nRe);  t0 = tic;  
for r = 1:nRe
    [Sol(r),X(:,r),FG(:,r),JB(r)] = PSH(PS,Data);
end, t1 = toc(t0);

