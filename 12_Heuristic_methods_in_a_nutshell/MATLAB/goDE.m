% goDE.m  -- version 2018-10-30
if ~exist('nG','var'),   nG = 40; end
if ~exist('CR','var'),   CR = .8; end
if ~exist('F','var'),     F = .8; end
if ~exist('nP','var'),   nP = 15; end
if ~exist('oneElemfromPv','var'), oneElemfromPv = 1; end
DE = struct('OF',OF,'SP',SP,'nG',nG,'CR',CR,'F',F,'nP',...
          nP,'oneElemfromPv',oneElemfromPv,'Restarts',nRe);
Sol = NaN(nRe,1);     X = zeros(Data.n,nRe);
FG  = zeros(nG,nRe); JB = NaN(1,nRe);  t0 = tic;  
for r = 1:DE.Restarts
    [Sol(r),X(:,r),FG(:,r),JB(r)] = DEH(DE,Data);
end, t1 = toc(t0);
