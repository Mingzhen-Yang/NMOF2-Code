function [F,X,Fbest,rbest] = SrealVec(n,nR,OF,Data)
% SrealVec.m -- version 2018-11-01 (Starting sol./pop.)
F = zeros(nR,1); X = zeros(n,nR);  int = Data.int; 
Fbest = realmax;
for r = 1:nR
    for i = 1:n
        lowlim = int(i,1); uplim = int(i,2);
        X(i,r) = lowlim + (uplim - lowlim) * rand;
        F(r) = feval(OF,X(:,r),Data);
        if F(r) < Fbest, Fbest = F(r); rbest = r; end
    end
end
