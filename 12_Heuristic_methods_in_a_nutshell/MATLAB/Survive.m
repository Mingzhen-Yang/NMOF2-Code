function P = Survive(GA,P1,P2,OF,Data)
% Survive.m  -- version 2018-08-23
nP = GA.nP;  nP1 = GA.nP1;  nP2 = GA.nP2;
P2.F = zeros(1,nP2);
for i = 1:nP2
    P2.F(i) = feval(OF,P2.C(i,:),Data); % Children fitness
end
F = [P1.F,P2.F];
[ignore,I] = sort(F);
for i = 1:nP
    j = I(i);
    if j <= nP1
        P.C(i,:) = P1.C(j,:);     % Surviving parents
        P.F(i) = P1.F(j);
    else
        P.C(i,:) = P2.C(j-nP1,:); % Suriving children
        P.F(i) = P2.F(j-nP1);
    end
end

