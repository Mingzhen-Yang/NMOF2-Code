function Data = SSPSS(OF,Data,nRe)
% SSPSS2.m  -- version 2018-10-03
n = Data.n;  k = fix(n/10);
xs = false(nRe,n); Fs = zeros(1,nRe); 
for r = 1:nRe
    m  = unidrnd(k,1) + k;
    J = randperm(n,m);
    xs(r,J) = true;
    Fs(r) = feval(OF,xs(r,:),Data);
end
Data.xs = xs;  Data.Fs = Fs;
