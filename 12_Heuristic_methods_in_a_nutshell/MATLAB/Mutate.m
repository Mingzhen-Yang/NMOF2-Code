function C = Mutate(GA,C)
% Mutate.m  -- version 2018-08-21
if rand < GA.pM
    % Child undergoes mutation
    j = unidrnd(GA.nC); % Select chromosome
    if ~C(j), C(j) = 1; else C(j) = 0; end
end
