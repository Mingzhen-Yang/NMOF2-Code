function C = Crossover(GA,P1)
% Crossover.m  -- version 2018-08-21
nP1 = GA.nP1;  nC = GA.nC;
par = unidrnd(nP1,2,1); % Select parents
p1 = par(1);   p2 = par(2);
k = unidrnd(nC - 1) + 1; % Select crossover
C = [P1.C(p1,(1:k-1)) P1.C(p2,k:nC)]; % crossover

