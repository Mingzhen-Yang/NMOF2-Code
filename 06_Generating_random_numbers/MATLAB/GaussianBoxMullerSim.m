function [z1,z2] = GaussianBoxMullerSim(N_samples)
% GaussianBoxMullerSim.m  -- version 2011-01-06
if nargin < 1, N_samples=1; end;
u = rand(N_samples,2);
z1 = sqrt(-2*log(u(:,1))) .* cos(2 * pi * u(:,2));
z2 = sqrt(-2*log(u(:,1))) .* sin(2 * pi * u(:,2));

