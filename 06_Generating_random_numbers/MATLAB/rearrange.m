function x = rearrange(x,s)
% rearrange.m  -- version 2011-01-06
%   x ... data sample
%   s ... length of segment to be moved

n  = length(x);
i_start = ceil(rand*(n-s+1));
i  = i_start + (0:(s-1));          % elements to be moved
t  = ceil(rand*(length(x)-s+1))-1; % target position
chunk = x(i);
x(i) = [];                         % remove chunk
x    = [x(1:t) chunk  x(t+1:end)]; % insert at new position