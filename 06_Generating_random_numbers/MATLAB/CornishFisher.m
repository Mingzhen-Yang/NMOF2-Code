function q = CornishFisher(r,alpha)
% CornishFisher.m  -- version 2011-01-06

S = skewness(r);
K = kurtosis(r);
u = norminv(alpha);
Omega = u + S/6*(u^2-1) + (K-3)/24 *(u^3 - 3*u) ...
        - S^2/36 * (2*u^3-5*u);
q = mean(r) + Omega * std(r);
