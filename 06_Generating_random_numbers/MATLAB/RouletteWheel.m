function w = RouletteWheel(prop,N)
% RouletteWheel.m  -- version 2011-01-06
% roulette wheel selection
% prop ... propensities for choosing an element ( > 0 )
% N ...... number of draws
if nargin < 2, N = 1; end

% -- compute (cumulative) probabilities
prob = max(0,prop)/sum(max(prop,0));
cum_prob = cumsum(prob);

% -- perform draws
w = NaN(1,N);
for i = 1:N
    u = rand;
    w(i) = find(u < cum_prob,1);
end