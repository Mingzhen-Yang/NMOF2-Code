function piEst = PibySimulation(N_samples)
% PibySimulation.m  -- version 2011-01-06

a = rand(N_samples,2)*2 - 1;
r = sqrt(a(:,1).^2 + a(:,2).^2);
within_unit_circle = (r<=1);
piEst = 4 * sum(within_unit_circle)/N_samples;