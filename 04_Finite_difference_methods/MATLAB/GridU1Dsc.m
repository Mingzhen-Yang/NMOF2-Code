function [Am,Ap,B,S] = GridU1Dsc(r,q,T,sigma,Smin,Smax,M,N,theta)
% GridU1Dsc.m  -- version  2011-05-14
% Uniform grid in original variables (sparse code)
f7 = 1;  dt = T/M;
S  = linspace(Smin,Smax,N+1)';
dS = (Smax - Smin) / N;
a = (dt*sigma^2*S(f7+(1:N-1)).^2) ./ (2*dS^2);
b = (dt*(r-q)*S(f7+(1:N-1))) / (2*dS);
d = a - b;   m = - 2*a - dt*r;   u = a + b;
%
tta = theta;
Am = [[0;   -tta*d(2:N-1)]     1-tta*m [   -tta*u(1:N-2);0]];
Ap = [[0;(1-tta)*d(2:N-1)] 1+(1-tta)*m [(1-tta)*u(1:N-2);0]];
B = [d(1) u(N-1)];
