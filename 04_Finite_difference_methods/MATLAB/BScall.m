function C = BScall(S,X,r,q,T,sigma)
% BScall.m  -- version  2007-04-05
% Pricing of European call with Black-Scholes
d1 = (log(S./X) + (r-q+sigma.^2 /2) .* T) ./ (sigma.*sqrt(T));
d2 = d1 - sigma .* sqrt(T);
C  = S .* exp(-q .* T) .* normcdf(d1) - ...
     X .* exp(-r .* T) .* normcdf(d2);
