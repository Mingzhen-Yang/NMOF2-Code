function [P,Svec] = FDM1DEuPut(S,X,r,q,T,sigma,Smin,Smax,M,N,theta)
%  FDM1DEuPut.m  -- version  2006-06-12
%  Finite difference method for European put
[Am,Ap,B,Svec] = GridU1D(r,q,T,sigma,Smin,Smax,M,N,theta);
V0 = max(X - Svec,0);  % Initial conditions
% Solve linear system for succesive time steps
[L,U] = lu(Am);  f7 = 1;  dt = T/M;
for j = M-1:-1:0
    V1 = V0;
    V0(f7+0) = (X-Svec(f7+0))*exp(-r*(T-j*dt));
    b = Ap*V1(f7+(1:N-1)) + theta *B*V0(f7+[0 N]) ...
                       + (1-theta)*B*V1(f7+[0 N]);
    V0(f7+(1:N-1)) = U\(L\b);
end
if nargout==2, P=V0; else P=interp1(Svec,V0,S,'spline'); end
