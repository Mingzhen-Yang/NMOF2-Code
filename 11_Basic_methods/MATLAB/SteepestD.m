function x1 = SteepestD(f,x1,tol,ls)
% SteepestD.m  -- version2010-08-31
if nargin == 2, tol = 1e-4; ls = 0.1; end
x1 = x1(:); x0 = -x1;  k = 1; 
while ~converged(x0,x1,tol)
    x0 = x1;
    g  = numG(f,x1);
    as = lineSearch(f,x0,-g,ls);
    x1 = x0 - as * g;
    k = k + 1; if k > 300, error('Maxit in SteepestD'); end
end
