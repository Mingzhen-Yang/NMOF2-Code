function F = OF(theta,y,X,b,c)
% OF.m  -- version 2010-11-11
r  = y - X*theta;
S0 = median(abs(r)) / b;
F  = FPIS(S0,r,c);
