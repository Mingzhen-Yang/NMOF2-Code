function x1 = BFGS(f,x1,tol,ls)
% BFGS.m  -- version  2010-09-14
if nargin == 2, tol = 1e-4; ls = 0.1; end
g1 = numG(f,x1);  B1 = eye(numel(x1));  
x1 = x1(:); x0 = -x1;  k = 1; 
while ~converged(x0,x1,tol)
    x0 = x1; g0 = g1; B0 = B1;
    p0 = -B0\g0;
    as = lineSearch(f,x0,p0,ls);
    s0 = as * p0;
    z0 = B0 * s0;
    x1 = x0 + s0;
    g1 = numG(f,x1);
    y0 = g1 - g0;
    B1 = B0 + (y0*y0')/(y0'*s0) - (z0*z0')/(s0'*z0);
    k = k + 1; if k > 100, error('Maxit in BFGS'); end
end
