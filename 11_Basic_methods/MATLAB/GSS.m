function c = GSS(f,a,b,tol)
% GSS.m  -- version 2006-05-26
% Golden Section Search
if nargin == 3, tol = 1e-8; end
tau = (sqrt(5) - 1)/2;
x1 = a + (1-tau)*(b-a); f1 = feval(f,x1);
x2 = a +    tau *(b-a); f2 = feval(f,x2);
while (b-a) > tol
    if f1 < f2
        b  = x2;
        x2 = x1;  f2 = f1;
        x1 = a + (1-tau)*(b-a); f1 = feval(f,x1);
    else
        a  = x1;
        x1 = x2;  f1 = f2;
        x2 = a +    tau *(b-a); f2 = feval(f,x2);
    end
end
c = a + (b-a)/2;
