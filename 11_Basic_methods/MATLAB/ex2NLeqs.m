function F = ex2NLeqs(x)
% ex2NLeqs.m  -- version 2010-12-23
F = zeros(numel(x),1);
F(1) = x(1) + x(2) - 3;
F(2) = x(1)^2 + x(2)^2 - 9;