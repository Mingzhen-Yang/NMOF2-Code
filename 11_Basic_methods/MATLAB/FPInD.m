% FPInD.m  -- version 2010-12-23
G = str2mat('-y0(2) + 3','sqrt(-y0(1)^2 + 9)');
y1 = [2 0]; y0 = -y1; tol = 1e-2; k = 1; itmax = 10; 
Y = NaN(itmax,2); Y(1,:) = y1';
while ~converged(y0,y1,tol)
    y0 = y1;
    y1(1) = eval(G(1,:));
    y1(2) = eval(G(2,:));
    k = k + 1;
    Y(k,:) = y1';
    if k > itmax, error('Iteration limit reached'); end
end
y1
