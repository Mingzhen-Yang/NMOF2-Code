function [f,g] = FnG(x,func)
% FnG.m  -- version 2010-12-21
f = func(x);
g = numG(func,x);