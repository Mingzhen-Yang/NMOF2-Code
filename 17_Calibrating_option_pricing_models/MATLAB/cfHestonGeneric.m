function cf = cfHestonGeneric(om,S,tau,r,q,param)
% cfHestonGeneric.m -- version 2010-10-04
% S     = spot
% tau   = time to mat
% r     = riskfree rate
% q     = dividend yield
% v0    = initial variance
% vT    = long run variance (theta in Heston's paper)
% rho   = correlation
% k     = speed of mean reversion (kappa in Heston's paper)
% sigma = vol of vol

v0    = param(1);
vT    = param(2);
rho   = param(3);
k     = param(4);
sigma = param(5);

d = sqrt( (rho * sigma * 1i*om - k).^2 + sigma^2 * ...
          (1i*om + om .^ 2) );
g = (k - rho*sigma*1i*om - d) ./ (k - rho*sigma*1i*om + d);
cf1 = 1i*om .* (log(S) + (r - q) * tau);
cf2 = vT * k / (sigma^2) * ((k - rho*sigma*1i*om - d) * ...
      tau - 2 * log((1 - g .* exp(-d * tau)) ./ (1 - g)));
cf3 = v0/sigma^2 * (k - rho*sigma*1i*om - d) .* ...
      (1 - exp(-d*tau)) ./ (1 - g .* exp(-d * tau));
cf  = exp(cf1 + cf2 + cf3);