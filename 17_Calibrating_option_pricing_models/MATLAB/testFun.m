% testFun.m -- version 2010-10-30
function z = testFun(x)
ind = NaN(length(x),1);
ind(x<=3 & x>=1)=0;ind(x<1)=-1;ind(x>3) = 1;
%
z(ind==0)  = 3-x(ind==0);
z(ind==-1) = x(ind==-1)+1;
z(ind==1)  = 2;
z=z';