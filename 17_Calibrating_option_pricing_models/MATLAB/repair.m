function sol = repair(sol, Data)
% repair.m  -- version 2011-01-16
hi = Data.Dc(:,2);
lo = Data.Dc(:,1);

% reflect around upper limit
a = sol - hi;
a = a + abs(a);
sol = sol - a;
% check if below lower boundary
sol = (sol + lo + abs(sol - lo))/2;

% reflect around lower limit
a = lo - sol;
a = a + abs(a);
sol = sol + a;
% check if above upper boundary
sol = (sol + hi - abs(sol-hi))/2;
end
