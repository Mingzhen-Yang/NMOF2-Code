function cf = cfBSMGeneric(om,S,tau,r,q,param)
% cfBSMGeneric.m -- version 2010-10-24
vT = param(1);
cf = exp(1i * om * log(S) + 1i * tau * (r - q) * ...
         om - 0.5 * tau * vT * (1i * om + om .^ 2));