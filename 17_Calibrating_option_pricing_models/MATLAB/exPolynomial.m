% exPoloynomial.m -- version 2011-01-01
% nice example
Fun1 = @(x)(exp(-x));
a = 0; b = 10; points = 200;
t = linspace(a,b,points);
x = Fun1(t);
plot(t,x);ylim([-0.5 1]);grid on, hold on
%
p = polyfit(t,x,5);f = polyval(p,t);
plot(t,f,'k--')

% ... fit polynomial
p2 = [t'.^0 t'.^1 t'.^2 t'.^3 t'.^4 t'.^5]\x';

% not so nice example 1
a = 0; b = 10*pi; points = 200;
t = linspace(a,b,points);
x = sin(t);
plot(t,x);ylim([-1.5 1.5]);grid on, hold on
%
p = polyfit(t,x,5);f = polyval(p,t);
plot(t,f,'k--');

% not so nice example 2
a = -5; b = 5; points = 200;
t = linspace(a,b,points);
x = testFun(t)';
plot(t,x);ylim([-4 4]);grid on; hold on
%
p = polyfit(t,x,5);f = polyval(p,t);
plot(t,f,'k--');