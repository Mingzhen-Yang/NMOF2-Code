function [x,w] = GLnodesweights(n)
% GLnodesweights  -- version 2010-10-24
% (G_auss L_egendre...)
eta = 1 ./ sqrt(4-(1:(n-1)).^(-2));
A = diag(eta,1) + diag(eta,-1);
[V,D] = eig(A);
x = diag(D);
% Matlab does not guaranty sorted eigenvalues
[x,i] = sort(x);
% weights: for Legendre w(x)=1; integral from -1 to 1 = 2
w = 2 * V(1,i) .^ 2;