function [xbest,Fbest,Fbv] = DE(OF,Data,P)
% DE.m -- version 2011-01-07
% settings for direct search
if P.NM > 0
    optionsA = optimset('Display','off','MaxIter',P.NMiter,...
                        'MaxFunEvals',P.NMiter);
    z = @(param)calibOF(param,Data);
end

% initialize matrices
Fbv   = NaN(P.nG,1);   % best F-value over generations
Fbest = realmax;       % best solution value
F = zeros(P.nP,1); %vector of F-values of members of population

% construct starting population
hi_lo = Data.Dc(:,2)-Data.Dc(:,1);
P1 = diag(hi_lo) * rand(Data.d,P.nP);
for i = 1:Data.d, P1(i,:) = P1(i,:) + Data.Dc(i,1); end

% ... and evaluate it
for i = 1:P.nP
    F(i) = feval(OF,P1(:,i),Data);
    if isnan(F(i)), F(i) = 1e7; end  % just in case...
    if F(i) < Fbest
        Fbest = F(i);
        xbest = P1(:,i);
    end
end

% start generations
for k = 1:P.nG
    P0 = P1;
    Io = randperm(P.nP)'; Ic = randperm(4)';
    R1 = circshift(Io,Ic(1));
    R2 = circshift(Io,Ic(2));
    R3 = circshift(Io,Ic(3));
    Pv = P0(:,R1) + P.F * (P0(:,R2) - P0(:,R3));% new solutions
    mPv = rand(Data.d,P.nP) < P.CR;             % crossover
    Pu = P0; Pu(mPv) = Pv(mPv);
    for i = 1:P.nP
        Pu(:,i) = repair(Pu(:,i),Data);
        Ftemp = feval(OF,Pu(:,i),Data);
        if Ftemp <= F(i)
            P1(:,i) = Pu(:,i);
            F(i) = Ftemp;
        end
    end
    % direct search
    if P.NM > 0
        if mod(k,P.NMmod) == 0
            if P.NMchoice == 1
                [ign,nnn] = sort(F);
            elseif P.NMchoice == 2
                nnn = randperm(P.nP);
            else
                error('choice not allowed')
            end
            for ni = 1:P.NMn
                auxF = F(nnn(ni)); diff = 1e7;
                while diff > P.NMpres
                    paramS = P1(:,nnn(ni));
                    NMsol = fminsearch(z,paramS,optionsA);
                    NMsol = repair( NMsol,Data);
                    Ftemp = calibOF(NMsol,Data);
                    if Ftemp < F(nnn(ni))
                        P1(:,nnn(ni)) = NMsol;
                        F(nnn(ni)) = Ftemp;
                        diff = abs(Ftemp-auxF);
                        auxF = Ftemp;
                    else
                        break
                    end
                end
            end
        end
    end
    % find best
    [Fbest,ibest] = min(F);
    Fbv(k) = Fbest;
    xbest = P1(:,ibest);
end
fprintf('standard dev. of solutions %4.3f\n',std(F))