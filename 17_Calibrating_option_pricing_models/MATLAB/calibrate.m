% calibrate.m -- version 2010-11-29
%% choose a model
model = 'heston';
%model = 'bates';
%model = 'bsm';
%model = 'merton';
%% set true parameters and compute true prices
S = 100; q = 0.02; r = 0.02;
XX = 75:5:125; TT = [1/12 3/12 6/12 9/12 1 2 3];
nS = length(XX); nT = length(TT); prices0 = NaN(nS,nT);
if strcmp(model,'heston')
    v0     = 0.2^2;    % current variance
    vT     = 0.2^2;    % long-run variance (theta in paper)
    rho    = -0.7;     % correlation
    k      = 1;        % mean reversion speed (kappa in paper)
    sigma  = 0.5;      % vol of vol
    for kk = 1:length(XX)
        for tt = 1:length(TT)
            prices0(kk,tt) = ...
                callHestoncf(S,XX(kk),TT(tt),r,q,v0,vT,rho,k,sigma);
        end
    end
    minP = [0.05^2  0.05^2 -1 0.01 0.01]';
    maxP = [0.90^2  0.90^2  1 5.00 5.00]';
elseif strcmp(model,'bates')
    v0     = 0.2^2;    % current variances
    vT     = 0.2^2;    % long-run variance (theta in paper)
    rho    = -0.3;     % correlation
    k      = 1.0;      % mean reversion speed (kappa in paper)
    sigma  = 0.3;      % vol of vol
    lambda = 0.2;      % intensity of jumps;
    muJ    = -0.1;     % mean of jumps;
    vJ     = 0.1^2;    % variance of jumps;
    for kk = 1:length(XX)
        for tt = 1:length(TT)
            prices0(kk,tt) = callBatescf(S,XX(kk),TT(tt), ...
                          r,q,v0,vT,rho,k,sigma,lambda,muJ,vJ);
        end
    end
    minP = [0.05^2  0.05^2 -1 0.01 0.01  0.00 -0.25  0.00^2]';
    maxP = [1.00^2  1.00^2  1 5.00 5.00  0.50  0.25  0.90^2]';
elseif strcmp(model,'bsm')
    v      = 0.2^2;    % variance
    for kk = 1:length(XX)
        for tt = 1:length(TT)
            prices0(kk,tt) = callBSMcf(S,XX(kk),TT(tt),r,q,v);
        end
    end
    minP = [0.01^2]';
    maxP = [2.00^2]';
elseif strcmp(model,'merton')
    v      = 0.2^2;    % variance (volatility squared)
    lambda = 0.2;      % intensity of jumps;
    muJ    = -0.03;    % mean of jumps;
    vJ     = 0.03^2;   % variance of jumps;
    for kk = 1:length(XX)
        for tt = 1:length(TT)
            prices0(kk,tt) = callMertoncf(S,XX(kk),TT(tt), ...
                                         r,q,v,lambda,muJ,vJ);
        end
    end
    minP = [0.05^2  0.00 -0.25 0.05^2]';
    maxP = [1.00^2  0.50  0.25 0.90^2]';
else
    error('model not specified')
end
% add noise
%prices0 = prices0 .* (randn(nS,nT)*0.001+1);

%% nodes/weights for integration
from = 0; to = 200; N = 50;
[x,w] = GLnodesweights(N);
[x,w] = changeInterval(x, w, -1, 1, from, to);
% collect all in Data structure
Data.model = model;
Data.S     = S;
Data.q     = q;
Data.r     = r;
Data.prices0 = prices0;
Data.XX    = XX;
Data.TT    = TT;
Data.x     = x;
Data.w     = w;
Data.Dc    = [minP maxP];
Data.d     = length(minP);
% DE settings (P1)
P1.nP = 25;   P1.nG = 50;
P1.CR = 0.95; P1.F  = 0.5;
P1.d = length(minP);
P1.NM = 1;         % do direct search (DS)? 1 = yes, 0 = no
P1.NMpres = 0.001; % when to stop DS
P1.NMiter =   100; % maximum iterations for DS
P1.NMmod = 10;     % how often to do DS (mod 10: every 10 it.)
P1.NMn = 2;        % how many searchers
P1.NMchoice = 1;   % what searchers: 1 = elite, 2 = random
% PS settings (P2)
P2.nP = 25; P2.nG = 50;
P2.d = length(minP);
P2.cv = 0.1;       % inital velocity
P2.vmax = 0.5;     % maximum (absolute) velocity
P2.iner = 0.7;     % inertia weight
P2.c1 = 1;         % weight personal best
P2.c2 = 2;         % weight alltime best
P2.NM = 1;         % do direct search (DS)? 1 = yes, 0 = no
P2.NMpres = 0.001; % when to stop DS
P2.NMiter =   100; % maximum iterations for DS
P2.NMmod = 10;     % how often to do DS (mod 10: every 10 it.)
P2.NMn = 2;        % how many searchers
P2.NMchoice = 1;   % what searchers: 1 = elite, 2 = random

% run DE
fprintf('\nDifferential Evolution \n')
tic, [solA,FbestA,FbvA] = DE(@calibOF,Data,P1);toc
[meanE,maxE] = calibOF(solA,Data)
% run PS
fprintf('\nParticle Swarm \n')
tic, [solB,FbestB,FbvB] = PSO(@calibOF,Data,P2);toc
[meanE,maxE] = calibOF(solB,Data)