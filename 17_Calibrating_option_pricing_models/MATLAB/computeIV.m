function iv = computeIV(S,X,tau,r,q,start,C)
% computeIV.m -- version 2010-10-24
% x is volatility; start is x0
diffF = @(x) callBSM(S,X,tau,r,q,x^2)-C;
iv = Newton0(diffF,start);

