function cf = cfMertonGeneric(om,S,tau,r,q,param)
% cfMertonGeneric.m -- version 2010-10-24
% S      = spot
% tau    = time to mat
% r      = riskfree rate
% q      = dividend yield
% v      = variance (volatility squared)
% -- jumps --
% lambda = intensity;
% muJ    = mean of jumps;
% vJ     = variance of jumps;
v        = param(1);
lambda   = param(2);
muJ      = param(3);
vJ       = param(4);
A = 1i*om*log(S) + 1i*om*tau*(r-q-0.5*v-lambda*muJ) ...
    - 0.5*(om.^2)*v*tau;
B = lambda*tau*(exp(1i*om*log(1+muJ) ...
    -0.5*1i*om*vJ-0.5*vJ*om.^2) - 1);
cf = exp(A + B);
