% exampleIV.m -- version 2010-10-26
S = 110;  % spot
X = 80;   % strike
r = 0.09; % interest rate
q = 0.00; % dividend
tau = 1;  % time to maturity

% compute a market price
trueVol = 0.3;
C = callBSM(S,X,tau,r,q,trueVol^2);

% ... and try to get trueVol back
start = sqrt(abs(log(S/X)+(r-q)*tau)*2/tau);
computeIV(S,X,tau,r,q,start,C)
